package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

var r chi.Router

func startServer() {
	r = chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	loadRoutes()
	http.ListenAndServe(":8080", r)
}

func loadRoutes() {
	r.Post("/", func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		w.Write([]byte(r.PostForm.Encode()))
	})
	r.Get(`/`, func(w http.ResponseWriter, req *http.Request) {
		playlist, _ := os.Open("playlist.json")
		data, _ := ioutil.ReadAll(playlist)
		log.Println(string(data))
		w.Write(data)
		defer playlist.Close()
	})
	r.Get("/v/{id}", func(w http.ResponseWriter, req *http.Request) {
		id := chi.URLParam(req, "id")
		for _, v := range list {
			if v.Vid == id {
				j := json.NewEncoder(w)
				j.Encode(v)
				return
			}
		}
		fmt.Fprint(w, "not found")
		return
	})
	r.Get("/g/{id}", func(w http.ResponseWriter, req *http.Request) {

	})
	r.Get("/download/v/{id}", func(w http.ResponseWriter, req *http.Request) {
		id := chi.URLParam(req, "id")
		for _, v := range list {
			if v.Vid == id {
				v.Save()
				return
			}
		}
		fmt.Fprint(w, "not found")
		return
	})
	r.Get("/download/g/{id}", func(w http.ResponseWriter, req *http.Request) {

	})

}
