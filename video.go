package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"time"
)

type video struct {
	Vid     string `json:"vid"`
	Pid     string `json:"pid"`
	Gid     string `json:"gid"`
	Type    string `json:"type"`
	Name    string `json:"name"`
	Title   string `json:"title"`
	URL     string `json:"url"`
	Enabled bool   `json:"enabled"`
	Path    string `json:"path"`
	Ext     string `json:"ext"`
}

func (v *video) download() (io.Reader, error) {
	client := http.Client{Timeout: time.Second * 5}
	res, err := client.Get(v.URL)
	if err != nil {
		return nil, err
	} else {
		defer res.Body.Close()
		return res.Body, nil
	}

}

func (v *video) createPath() {
	err := os.MkdirAll(v.Path, os.FileMode(0777))
	if err != nil {
		fmt.Println(err)
	}
}
func (v *video) Save() {
	v.createPath()
	data, err := v.download()
	if err != nil {
		fmt.Println(err)
	} else {
		file := v.Path + v.Title + `.` + v.Ext
		out, err := os.Create(file)
		if err != nil {
			fmt.Println(err)
		}
		io.Copy(out, data)
		defer out.Close()
	}

}
func (v *video) delete() {
	v.Enabled = false
}
func (v *video) SetUrl(u string) {
	v.URL = u
	chunks := strings.Split(v.URL, `/`)
	tmp := strings.Split(chunks[6], `.`)
	v.Type = chunks[3]
	v.Gid = chunks[4]
	v.Pid = chunks[5]
	v.Vid = tmp[0]
	v.Ext = tmp[1]
	v.setPath()
}
func (v *video) SetName(n string) {
	v.Name = formatName(n)
}
func (v *video) SetTitle(t string) {
	v.Title = formatTitle(t)
}
func formatTitle(title string) string {
	//chunks := strings.Split(title, `"`)[1]
	replacer := strings.NewReplacer(`:`, `-`, `"`, ``, `,`, ``)
	return replacer.Replace(title)
}
func formatName(name string) string {
	//chunks := strings.Split(name, `"`)[1]
	replacer := strings.NewReplacer(`:`, `-`, `"`, ``, `,`, ``)
	return replacer.Replace(name)
}
func (v *video) setPath() {
	defer func() {
		recover()
	}()
	if v.Type != "movie" {
		subfolder := strings.Split(v.Title, `S0`)[1]
		sub := []byte(subfolder)[0:1]
		t := `S0` + fmt.Sprintf("%s", sub)
		v.Path = `./serie/` + v.Name + `/` + t + `/`
	} else {
		v.Path = `./movie/` + v.Name + `/`
	}
}
