package main

import (
	"bufio"
	"encoding/json"
	"os"
	"strings"
)

var list []video
var playlist *os.File

func main() {
	file, _ := os.Open("playlist.m3u")
	scanner := bufio.NewScanner(file)
	var v video
	defer file.Close()
	for scanner.Scan() {
		line := scanner.Text()
		if strings.Contains(line, "#EXTINF") {
			if strings.Contains(line, `"`) {
				chunks := strings.Split(line, `"`)
				v.SetName(chunks[1])
				v.SetTitle(chunks[2])
			} else {
				chunks := strings.Split(line, `,`)
				v.SetName(chunks[1])
				v.SetTitle(chunks[2])
			}

		}
		if strings.Contains(line, "http") && !strings.Contains(line, ".ts") {
			v.SetUrl(line)
			//	v.Save()
			list = append(list, v)
		}
	}
	playlist, _ = os.Create("playlist.json")
	encoder := json.NewEncoder(playlist)
	encoder.Encode(list)
	startServer()
	defer playlist.Close()
}

// func GenerateID() string {
// 	b := make([]byte, 16)
// 	_, err := rand.Read(b)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	uuid := fmt.Sprintf("%x-%x-%x-%x-%x", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])
// 	return uuid
// }
